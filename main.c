#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ip.h"
#include "validate.h"
#include "fichier.h"
#include "masque.h"
#include "error.h"

int main() {
//déclaration des variables
   char adresse[20]; //contient l'adresse de départ
   int ipv4[4];//contient les 4 parties de l'ip
   int reseau[4];//contient les 4 parties de l'adresse masque
   int p_rest[4];//contient les 4 parties de la partie restante calculé avec l'adresse du masque
   int s_reseau[4];//contient les 4 parties de l'adresse reseau
   int hote[4];//contient les 4 parties de l'adresse hôte
   char *ip; //partie ip avant découpage et numérisation
   char *mask;//partie masque avant numérisation
   char *ch;//tampon de l'adresse ip de départ
   int slash=-1;//indice de découpage et de présence du masque
   char *classe;//classe de l'ip
   char *type;//type de l'ip
   int masque=0;//valeur numérisé du masque

   for (int i=0;i<4;i++){ //initialiser les parties de l'ip pour faciliter la détection des erreurs
      ipv4[i]=-1;
   }

//entrée de l'adresse IP
   printf("Saisissez l'ip souhaitée(avec ou sans masque au format IPV4):\n");
   scanf("%s", adresse);//lecture de l'adresse ip
   ch = adresse; // mise en mémoire de l'adresse dans le tampon


//vérification du format
 //segmentation ip/masque
  separation_format(ch, &ip, &mask, &slash);
  //validation du format
  switch (validation_format(&ip,&mask,&slash,ipv4)){ //verification du format de l'adresse renseignée

   case 1://adresse ip et masque renseigné correcte
      masque = atoi(mask); //numérisation du masque
      classe=classe_ip(ipv4[0]);//attribution de la classe de l'ip
      type=type_ip(ipv4[0],ipv4[1],ipv4[2],ipv4[3]);//attribution (si possible) du type de l'ip
      masque_reseau(masque,reseau);//passage du format CIDR du masque sous forme d'adresse ipv4
      adresse_reseau(ipv4,reseau,s_reseau);//calcul de l'adresse reseau avec l'adresse ip et l'adresse masque
      calc_partie_restante(reseau,p_rest);//calcul de la partie restante qui permet de calculer l'adresse hote (avec l'adresse masque)
      adresse_reseau(ipv4,p_rest,hote);//calcul de l'adresse hote (avec l'adresse de la partie restante et l'adresse ip)
      creation_fichier(ipv4,masque,hote,s_reseau,reseau,p_rest,classe,type); //creation du fichier resultat
      afficher_fichier();//affichage du fichier resultat
      break;

   case 2://adresse ip renseigné seule correcte
      classe=classe_ip(ipv4[0]);//attribution de la classe de l'ip
      type=type_ip(ipv4[0],ipv4[1],ipv4[2],ipv4[3]);//attribution (si possible) du type de l'ip
      masque=attribution_masque_classe(ipv4[0]);//attribution (si possible) du masque en fonction de la classe de l'ip
      if(masque!=0){
         masque_reseau(masque,reseau);//passage du format CIDR du masque sous forme d'adresse ipv4
         adresse_reseau(ipv4,reseau,s_reseau);//calcul de l'adresse reseau avec l'adresse ip et l'adresse masque
         calc_partie_restante(reseau,p_rest);//calcul de la partie restante qui permet de calculer l'adresse hote (avec l'adresse masque)
         adresse_reseau(ipv4,p_rest,hote);//calcul de l'adresse hote (avec l'adresse de la partie restante et l'adresse ip)
      }
      creation_fichier(ipv4,masque,hote,s_reseau,reseau,p_rest,classe,type);//creation du fichier resultat
      afficher_fichier();//affichage du fichier resultat
      break;

   default://erreur de format
      creation_fichier_erreur(ipv4,&mask,&slash);//création du fichier erreurs
      afficher_fichier();//affichage du fichier erreurs
      break;
   }
}