# Adresse IP - M2101

Mini projet pour le cours de M2101
Julien Davidou
Antoine Beyssen


Vous créerez un Readme dans votre dépôt git dans lequel vous présenterez en quelques lignes l'objectif de l'application développée, vos NOM et Prénom, La documentation de chaque fonction (paramètres d'entrées, sorties), les cas d'erreur,...

L'objectif de l'application développée et de recevoir en entrée une adresse IP et d'en donner son type, sa classe, l'adresse du réseau ainsi que celle de l'hôte (si elle existe). Tout cela en vérifiant la conformité de l'adresse vis à vis des normes d'écriture de l'ipv4 (X.X.X.X/D). Avec un X compris entre 0 et 255, et D un masque au format CIDR compris entre 1 et 32.

# validate.c 
    
    // validate_number

Prend en entrée une chaîne de caractères et vérifie que chaque caractère est un nombre.
Retourne 1 si tous les caractères sont des nombres, 0 sinon.
Fonctions utilisées : isdigit

    // validate_ip

Prend en entrée une chaîne de caractères correspondant à l'adresse IP, un tableau d'entiers ainsi que la taille de la chaîne.  
Retourne 1 si la chaîne de caractères est confomre à la norme IPv4, 0 sinon.
Il sépare aussi en 4 partie l'ip après validation que la chaîne de caractère étudiée est un nombre. (stocké dans le tableau d'entier nommé ipv4)
Fonctions utilisées : validate_number, strtok, atoi

    // validate_mask

Prend en entrée une chaîne de caractères correspondant au masque de l'adresse IP, et vérifie son format (si c'est un nombre et si il est correcte).
Retourne 1 si le masque est correct, 0 sinon.
Fonctions utilisées : validate_number, atoi

    // validation_format

Prend en entrée 2 chaînes de caractères, un correspondant à l'adresse IP seule, l'autre son masque, ainsi que l'indice de découpage/présence du masque, et un tableau d'entiers. Vérifie le format complet de l'adresse IP.
Retourne 2 si l'adresse estt correcte mais n'a pas de masque, 1 si l'adresse et son masque sont corrects, et 0 sinon.
Fonctions utilisées : validate_ip, validate_mask

    // separation_format
Prend en entrée 1 chaîne de caractères et 2 chaînes en sorti ainsi que l'indice de découpage/présence du masque qui va compter le nombre de "/". Sépare l'adresse d'entrée en deux chaînes de caractères, l'une correspondant à l'IP seule, l'autre à son masque seul.
Fonctions utilisées : strtok

# ip.c

    // classe_ip

Prend en entrée un entier correspodant aux 3 premiers chiffres de la première partie de l'IP. Retourne la classe de l'adresse sous forme de caractère (ex : retourne "A" si l'IP est de classe A).

    // type_ip

Prend en entrée 4 entiers, chacun correspondant à l'une des 4 parties de l'IP. 
Retourne le type de l'adresse sous forme de chaîne de caractères (ex : retourne "Publique" si l'IP est de type publique).

    // attribution_masque_classe

Prend en entrée un entier correspodant aux 3 premiers chiffres de la première partie de l'IP. Attribue, si possible, un masque à l'IP en fonction de cet entier.
Retourne 8, 16, 24 si l'attribution et possible et que les conditions sont respectées, ou 0 si l'attribution n'est pas possible. 

# masque.c

    // masque_reseau

Prend en entrée 2 entiers, le masque et l'adresse du masque. 
Calcule l'adresse masque suivant le format CIDR.

    // adresse_reseau
Prend en entrée 2 tableaux d'entiers, l'adresse IP, l'adresse masque, et renvoie l'adresse de sous réseau.
Calcule une adresse en faisant le ET logique de deux adresses données.

    // calc_partie_restante
Calcule la partie restante d'une adresse de masque.

# fichier.c

    // creation_ficher

Création du fichier résultat 'ip.txt' à partir de l'adresse IP traitée lors de l'exécution qui a été décomposée. Ainsi que des adresses calculés par la suite(masque,sous_reseau,hôte) si le masque est renseigné ou rajouté ainsi que la classe et le type de l'ip.
Fonctions utilisées : fopen, fprintf, fclose
    
    // afficher_fichier

Affichage du fichier résultat 'ip.txt'
Fonctions utilisées : fopen, printf, getline, feof, free, fclose

# erreur.c

    // creation_fichier_erreur

Crée un fichier contenant les erreurs dans le cas d'une adresse invalide. (si validate_format retourne 0)
Fonctions utilisées : fopen, fprintf, fclose

# main.c

Fichier d'execution, reprenant chacune des fonctions détaillées ci-dessus et gère l'entrée de l'ip ainsi que les différents cas retournés par validate_format.

Cas d'erreurs:
    La partie est n'est pas composée de 4 parties.
    Une de ces parties n'est pas un chiffre ou qu'il n'est pas compris entre 0 et 255.
    Le masque n'est pas une valeur numérique.
    Sa valeur n'est pas comprise entre 1 et 32.